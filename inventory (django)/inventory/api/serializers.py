from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import Phone
from .models import Laptop


class PhoneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Phone
        fields = ['id','name', 'model_id','year']

class LaptopSerializer(serializers.ModelSerializer):
    class Meta:
        model = Laptop
        fields = ['id','name', 'model_id', 'year']