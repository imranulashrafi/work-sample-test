from django.db import models

class Phone(models.Model):
    name = models.CharField(max_length=32)
    model_id = models.CharField(max_length=32)
    year = models.IntegerField()

class Laptop(models.Model):
    name = models.CharField(max_length=32)
    model_id = models.CharField(max_length=32)
    year = models.IntegerField()


