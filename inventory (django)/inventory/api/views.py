from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from .serializers import PhoneSerializer
from .serializers import LaptopSerializer
from .models import Phone
from .models import Laptop

from django.http import HttpResponse

def index(request):
    html= '<h1><a href="/admin">Admin Panel</a></h1>'
    return HttpResponse(html)

class PhoneViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Phone.objects.all()
    serializer_class = PhoneSerializer

class LaptopViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Laptop.objects.all()
    serializer_class = LaptopSerializer