import { Component } from '@angular/core';
import { ApiService } from './api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ApiService]
})
export class AppComponent {
  phones = [];
  laptops = []

  constructor(private api:ApiService) {
    this.getPhones();
    this.getLaptops();

  }

  getPhones = () =>{
    this.api.getAllPhones().subscribe(
      data => {
        this.phones = data;
      },
      error => {
        console.log(error);
      }
    )
  }
  getLaptops = () =>{
    this.api.getAllLaptops().subscribe(
      data => {
        this.laptops = data;
      },
      error => {
        console.log(error);
      }
    )
  }

}
