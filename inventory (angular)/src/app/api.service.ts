import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  baseurl = "https://myworstinventory.herokuapp.com";
  httpHeaders = new HttpHeaders({'Content-Type': 'application/json'})

  constructor(private http: HttpClient) { }
  getAllPhones(): Observable<any>{
    return this.http.get(this.baseurl+'/phones/', {headers: this.httpHeaders})
  }
  getAllLaptops(): Observable<any>{
    return this.http.get(this.baseurl+'/laptops/', {headers: this.httpHeaders})
  }

}



