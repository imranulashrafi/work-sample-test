# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/core/actions/#custom-actions/


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
import requests


class ActionShowInventory(Action):

    def name(self) -> Text:
        return "action_inventory"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        res1 = requests.get('https://myworstinventory.herokuapp.com/phones', headers={'content-type': 'application/json'})
        res2 = requests.get('https://myworstinventory.herokuapp.com/laptops', headers={'content-type': 'application/json'})
        # print(res1.json())
        dispatcher.utter_message(text="Phones")
        for x in res1.json():
            dispatcher.utter_message(text=str(x))
        dispatcher.utter_message(text="Laptops")
        
        for y in res2.json():
            dispatcher.utter_message(text=str(y))


        return []
